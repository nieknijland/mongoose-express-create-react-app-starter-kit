const express = require('express');

const router = express.Router();

// Do work here
router.get('/starter-kit-message', (req, res) => {
  res.json('Setup starter kit properly 😎');
});

module.exports = router;

