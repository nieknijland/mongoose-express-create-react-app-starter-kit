# mongoose-express-create-react-app-starter-kit

Project starter kit. Copy this repo to start a fresh project with mongoose, express and create-react-app

This project uses:
- Express setup strongly based on Wes Bos' [Learn node](https://wesbos.com/learn-node/).
- [Create react app](https://github.com/facebook/create-react-app)

## Start development

1. Copy `variables.env.example` and rename it to `variables.env`.

2. Add a random string to `KEY` and `SECRET` in `variables.env`.

3. Run: `docker-compose up`

4. Run: `yarn && yarn start`

5. Go to the folder `www/` and run: `yarn && yarn start`

6. In your browser go to: `http://localhost:3000`. If you see the message `Setup starter kit properly 😎` underneatch the default create-react-app message you're ready to start developping.
